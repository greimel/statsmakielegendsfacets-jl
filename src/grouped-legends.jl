using StatsMakie, Makie, Random
N = 100
x = randn(N)
y = randn(N)
g1 = rand(["color $i" for i in 1:3], N)
g2 = rand(["marker $i" for i in 1:3], N)

# Groups
## Legend with just one group works
scene = scatter(Group(g1), x, y)
leg = AbstractPlotting.legend(scene[end].plots, unique(g1), camera=campixel!, raw=true)
vbox(scene, leg)

scene = plot(density, Group(g1), x)
leg = AbstractPlotting.legend(scene[end].plots, unique(g1), camera=campixel!, raw=true)
vbox(scene, leg)

## The default legend with two groups is not what we like
scene = scatter(Group(color=g1, marker=g2), x, y, markersize=0.2)
leg = AbstractPlotting.legend(scene[end].plots, unique(g1 .* g2), camera=campixel!, raw=true)
vbox(scene, leg)

# TODO: ordering of labels

## Improved version
using DataFrames, IterableTables
scene.plots[end].input_args
_is_group(x) = typeof(x.val) <: Group
_is_xyz(x) = typeof(x.val) <: Array

function grouped_legend!(scene)
  input_args = scene.plots[end].input_args
  ## Get group info
  i_grp = findfirst(_is_group.(input_args))
  grp = input_args[i_grp].val.columns
  ## Get data
  xyz = input_args[collect(_is_xyz.(input_args))]

  leg = mapreduce(hbox, zip(keys(grp), grp)) do n_grp
    nt = NamedTuple{(n_grp[1],)}((n_grp[2],))
    @show typeof(nt)
    plt_type = typeof(scene[end].plots[1])
    if plt_type <: Lines
      scn = lines(Group(nt), xyz...)
    elseif plt_type <: Scatter
      scn = scatter(Group(nt), xyz...)
    else
      @error("bla")
    end
    scn
    @show labels = unique(nt[1])
    leg = legend(scn[end].plots, labels, camera=campixel!, raw=true)
  end

  vbox(scene, leg)
end

legend(grouped_legend!(scene)[1][end].plots, string.(1:3), camera=campixel!, raw=true)

scene = plot(StatsMakie.density, Group(color=g1, linestyle=g2), x)
grouped_legend!(scene)

scene = scatter(Group(color=g1, marker=g2), x, y)
grouped_legend!(scene)

# Random walks
ts = vec(cumsum(randn(N, 9), dims=1))
g00 = [i for i in 1:9 for j in 1:N]
g01 = ["foo $i" for i in 1:3 for j in 1:N for k in 1:3]
g02 = ["bar $i" for j in 1:3 for i in 1:3 for k in 1:N]

scene = lines(Group(color=g01, linestyle=g02), ts)
grouped_legend!(scene)

## Styled legend (only color for now)
p1 = scatter(Style(color=x), x, y)


cm = colorlegend(
    p1[end],             # access the plot of Scene p1
    raw = true,          # without axes or grid
    camera = campixel!,  # gives a concrete bounding box in pixels
                         # so that the `vbox` gives you the right size
    width = (            # make the colorlegend longer so it looks nicer
        30,              # the width
        300              # the height
    )
    )

vbox(p1, cm)

