using StatsMakie, Random, DataFrames
N = 100
x = randn(N)
y = randn(N)
g1 = rand(["color $i" for i in 1:3], N)
g2 = rand(["marker $i" for i in 1:3], N)

df = DataFrame(x=x, y=y, g1=g1, g2=g2)

scene = scatter(Data(df), Group(color=:g1), :x, :y, title="bla")

map(groupby(df, :g1)) do subdf
  scn = scatter(Data(subdf), :x, :y)
  #AbstractPlotting.title(sc, string(subdf[:g1][1]))
end |> DataFrame |> d -> vbox(d.x1)

AbstractPlotting()